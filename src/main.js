const express = require('express')

const app = express()
const server = require('http').createServer(app)
const morgan = require('morgan')
const bodyParser = require('body-parser')
const routers = require('./router/MainRouter')
const env = require('./private').environment
require('./supports/Extension')

const port = 3000
server.listen(port)

process.env.NODE_ENV = env

app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: true }))
app.use(routers)
app.use(morgan(':method :url :status :res[content-length] - :response-time ms'))

console.log('Server listening on port %s', port)
console.log(`Start at ${new Date()}`)
