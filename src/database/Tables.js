module.exports = {
    tbUser: 'User',
    tbDevice: 'Device',
    tbSocialAccount: 'SocialAccount',
    tbMagicCode: 'MagicCode',
    tbSponsor: 'Sponsor',
    tbBankAccount: 'BankAccount',
    tbBank: 'Bank',
}
