const MongoClient = require('mongodb').MongoClient
const { url, db_name } = require('../private').mongo

let database
MongoClient.connect(url, { useNewUrlParser: true }, (err, db) => {
    if (err) throw err
    const dbo = db.db(db_name)
    database = dbo
})

async function insert(collection_name, insert_data, callback) {
    database.collection(collection_name).insertOne(insert_data, (insert_err, result) => {
        if (insert_err) throw insert_err
        callback(result)
    })
}

async function insertMany(collection_name, insert_data, callback) {
    database.collection(collection_name).insertMany(insert_data, (insert_err, result) => {
        if (insert_err) throw insert_err
        if (typeof callback === 'function') {
            callback(result)
        }
    })
}


module.exports = {
    insert,
    insertMany,
}
