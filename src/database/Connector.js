const Promise = require('promise')
const mysql = require('mysql')
const environment = require('../private').db

function createConnection() {
    return mysql.createConnection(environment)
}

async function executeWithConnection(query_string, connection) {
    return new Promise((resolve, reject) => {
        let query = query_string
        query = query.replace(/"undefined"/g, connection.escape(null))
        query = query.replace(/undefined/g, connection.escape(null))
        connection.query(query, (err, results, fields) => {
            connection.end()
            const message = `${query}`
            console.log(message)
            if (err != null) {
                console.log(`fail with error: \n ${err}`)
                reject(err)
            } else {
                console.log('success')
                resolve(results, fields)
            }
            console.log('- - - - - - - - - - - -')
        })
    })
}

async function execute(query_string) {
    const connection = createConnection()
    return executeWithConnection(query_string, connection)
}

async function executeInTransaction(query_strings) {
    return new Promise((resolve, reject) => {
        const connection = createConnection()
        connection.beginTransaction(async (err) => {
            if (err) {
                reject(err)
                return
            }
            for (let i = 0; i < query_strings.length; i += 1) {
                await executeWithConnection(query_strings[i], connection)
            }

            connection.commit((commit_err) => {
                if (commit_err) {
                    connection.rollback(() => { })
                    reject(err)
                    return
                }
                connection.end()
                resolve(true)
            })
        })
    })
}

module.exports = {
    execute,
    executeInTransaction,
}
