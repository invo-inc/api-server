const Key = require('../../supports/Setting').stripe_sk
const Stripe = require('stripe')(Key)

async function createAccount(email, user_id) {
    const description = `Customer for user: ${user_id} - ${email}`
    const result = await Stripe.customers.create({ description, email })
    return result.id
}

async function createBank(account_id, account_holder_name, account_number, routing_number, country, currency) {
    const data = {
        object: 'bank_account',
        country: country.toLowerCase(),
        currency: currency.toUpperCase,
        account_holder_name,
        account_number,
        routing_number,
        account_holder_type: 'individual',
    }

    try {
        const result = await Stripe.customers.createSource(account_id, { source: data })
        return result.id
    } catch (error) {
        return error
    }
}

module.exports = {
    createAccount,
    createBank,
}
