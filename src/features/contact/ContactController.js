const ContactDb = require('./ContactDb')

async function addContacts(req, res) {
    const user_id = req.user_id
    const data = req.body.data
    for (let i = 0; i < data.length; i += 1) {
        const element = data[i]
        element.user_id = user_id
    }
    ContactDb.saveContacts(user_id, data)
    res.send({ success: true })
}

module.exports = {
    addContacts,
}
