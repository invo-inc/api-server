const { Mongo } = require('../../supports/Importer')

async function saveContacts(user_id, contacts) {
    Mongo.insertMany('contact', contacts)
}

module.exports = {
    saveContacts,
}
