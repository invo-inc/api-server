const Helper = require('../../supports/Helpers')
const SponsorDb = require('./SponsorDb')
const Emailer = require('../email/EmailController')
const Status = require('../../supports/Enum').SponsorStatus

function validateSponsor(email, to_user_id, full_name, percentage, relationship, res) {
    const user_id = to_user_id || 0
    if (user_id === 0) { // unregistered user
        if (Helper.isValidEmail(email) === false) {
            res.error(400, { error: 'Your email is invalid' })
            return false
        }

        if (Helper.isValidString(full_name) === false) {
            res.error(400, { error: 'Your full name is needed' })
            return false
        }
    }

    const percent = percentage || 0
    if (percent === 0) {
        res.error(400, { error: 'Percentage of earning is needed' })
        return false
    }

    if (Helper.isValidString(relationship) === false) {
        res.error(400, { error: 'Relationship is needed' })
        return false
    }

    return true
}

async function sendEmailToSponsor(email, to_user_name, user_name, percentage) {
    const content = `${user_name} has just added you as a sponsor with ${percentage}% of your earning at INVO. 
    
    Join INVO, start earning, and sponsor ${user_name}
    Download INVO on [Appstore] or [Google Play Store]`
    Emailer.sendSponsorship(email, to_user_name, content)
}

async function pushNotification(from_user_id, to_user_id, percentage) {
    // later
}

async function addSponsor(req, res) {
    const user_id = req.user_id
    const data = req.body
    const email = data.email || ''
    const to_user_id = data.to_user_id
    const to_user_name = data.name || ''
    const percentage = data.percentage || 0
    const relationship = data.relationship || ''

    if (user_id === to_user_id) {
        res.error(403, { error: 'You can\'t request sponsor to yourself' })
        return
    }

    const is_valid = validateSponsor(email, to_user_id, to_user_name, percentage, relationship, res)
    if (is_valid === false) { return }
    const result = await SponsorDb.addSponsor(user_id, to_user_id, email, to_user_name, percentage, relationship)
    if (result === false) {
        res.error(500, { success: false })
        return
    }
    res.send({ success: true })
    if (to_user_id === 0) {
        const from_user_name = await SponsorDb.getUserName(user_id)
        sendEmailToSponsor(email, to_user_name, from_user_name, percentage)
    } else {
        pushNotification(user_id, to_user_id, percentage)
    }
}

async function resendRequestSponsor(req, res) {
    const user_id = req.user_id
    const sponsor_id = req.params.id
    const sponsor_detail = await SponsorDb.getSponsorDetail(sponsor_id)
    if (sponsor_detail.resend_count >= 2) {
        res.error(403, { error: 'You can\'t request to this user anymore.' })
        return
    }

    if (sponsor_detail.to_user_id === null) {
        const from_user_name = await SponsorDb.getUserName(user_id)
        const { email, full_name: to_user_name, percentage } = sponsor_detail
        sendEmailToSponsor(email, to_user_name, from_user_name, percentage)
    } else {
        const { to_user_id, percentage } = sponsor_detail
        pushNotification(user_id, to_user_id, percentage)
    }
    res.send({ success: true })
    SponsorDb.increaseResendCount(sponsor_id)
}

async function getSponsors(req, res) {
    const user_id = req.user_id
    const sponsors = await SponsorDb.getList(user_id)
    res.send(sponsors)
}

async function remove(req, res) {
    const user_id = req.user_id
    const sponsor_id = req.params.id
    const can_change = await SponsorDb.canChange(user_id, sponsor_id)
    if (can_change === false) {
        res.error(403, { error: 'You have no permission to remove this sponsor' })
        return
    }

    const success = await SponsorDb.removeSponsor(sponsor_id)
    res.send({ success })
}

async function respond(req, res, status) {
    const user_id = req.user_id
    const sponsor_id = req.params.id
    const can_change = await SponsorDb.canChange(user_id, sponsor_id)
    if (can_change === false) {
        res.error(403, { error: 'You have no permission to respond this request' })
        return
    }

    const success = await SponsorDb.respond(sponsor_id, status)
    res.send({ success })
}

async function acceptRequest(req, res) {
    respond(req, res, Status.accepted)
}

async function rejectRequest(req, res) {
    respond(req, res, Status.rejected)
}

async function cancelRequest(req, res) {
    respond(req, res, Status.cancelled)
}

async function findUserByEmail(req, res) {
    const email = req.query.email
    const user = await SponsorDb.findUser(email)
    if (user === null) { res.send({ exist: false }); return }
    res.send(user)
}

module.exports = {
    addSponsor,
    getSponsors,
    remove,
    acceptRequest,
    rejectRequest,
    cancelRequest,
    findUserByEmail,
    resendRequestSponsor,
}
