const { Database, Tables } = require('../../supports/Importer')
const { SponsorStatus, SponsorType } = require('../../supports/Enum')

async function addSponsor(user_id, to_user_id, email, full_name, percentage, relationship) {
    const query = `insert into ${Tables.tbSponsor} (user_id, to_user_id, email, full_name, percentage, relationship, created_at) 
    values (${user_id}, ${to_user_id}, '${email}', '${full_name}', ${percentage}, '${relationship}', '${new Date().unix()}')`
    const result = await Database.execute(query)
    return result.affectedRows === 1
}

async function getUserName(user_id) {
    const query = `select first_name, last_name from ${Tables.tbUser} where user_id = ${user_id}`
    const result = await Database.execute(query)
    if (result.length === 0) { return 'Someone' }
    const user = result[0]
    const name = `${user.first_name} ${user.last_name}`
    return name
}

function getActiveStatusQuery() {
    return `(status = '${SponsorStatus.accepted}' or status = '${SponsorStatus.pending}')`
}

async function getList(user_id) {
    const activeStatus = getActiveStatusQuery()
    let query = `select id, s.user_id, to_user_id, u.email, first_name, last_name, avatar, relationship, percentage, s.status, s.created_at 
    from ${Tables.tbUser} u, ${Tables.tbSponsor} s where s.to_user_id = u.user_id and u.user_id in 
    (select to_user_id from ${Tables.tbSponsor} s where s.user_id = ${user_id} and to_user_id is not null and ${activeStatus})`
    const registered_users = await Database.execute(query)

    query = `select id, s.user_id, to_user_id, email, full_name, relationship, percentage, status, created_at from ${Tables.tbSponsor} s where s.user_id = ${user_id} and to_user_id is null and ${activeStatus}`
    const unregistered_user = await Database.execute(query)

    query = `select id, s.user_id, to_user_id, email, full_name, relationship, percentage, status, created_at from ${Tables.tbSponsor} s where s.to_user_id = ${user_id} and ${activeStatus}`
    const your_sponsorship = await Database.execute(query)

    const list = registered_users.concat(unregistered_user).concat(your_sponsorship)
    list.sort((a, b) => a.created_at - b.created_at)

    for (let i = 0; i < list.length; i += 1) {
        const item = list[i]
        if (item.user_id === user_id) {
            item.type = SponsorType.sponsor
        } else if (item.to_user_id === user_id) {
            item.type = SponsorType.sponsorship
        }
    }

    return list
}

async function respond(sponsor_id, action) {
    const query = `update ${Tables.tbSponsor} set status = '${action}' where id = ${sponsor_id}`
    const result = await Database.execute(query)
    return result.affectedRows === 1
}

async function canChange(user_id, sponsor_id) {
    const query = `select count(email) > 0 can_change from ${Tables.tbSponsor} where id = ${sponsor_id} and user_id = ${user_id}`
    const result = await Database.execute(query)
    return result.can_change
}

async function findUser(email) {
    const query = `select first_name, last_name, avatar, user_id from ${Tables.tbUser} where email = '${email}'`
    const result = await Database.execute(query)
    if (result.length === 0) { return null }
    return result[0]
}

async function getSponsorDetail(sponsor_id) {
    const query = `select id, user_id, to_user_id, email, full_name, percentage, relationship, status, notes, resend_count from ${Tables.tbSponsor} where id = ${sponsor_id}`
    const result = await Database.execute(query)
    if (result.length === 0) { return null }
    return result[0]
}
async function removeSponsor(sponsor_id) {
    const query = `update ${Tables.tbSponsor} set status = '${SponsorStatus.removed}' where id = ${sponsor_id}`
    const result = await Database.execute(query)
    return result.affectedRows === 1
}

async function increaseResendCount(sponsor_id) {
    const query = `update ${Tables.tbSponsor} set resend_count = resend_count + 1 where id = ${sponsor_id}`
    Database.execute(query)
}

module.exports = {
    addSponsor,
    getUserName,
    getList,
    respond,
    canChange,
    findUser,
    getSponsorDetail,
    removeSponsor,
    increaseResendCount,
}
