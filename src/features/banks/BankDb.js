const { Database, Tables } = require('../../supports/Importer')
const Stripe = require('../payment_gateway/Stripe')

async function addBankAccount(user_id, bank_name, account_holder_name, account_number, routing_number, country, currency, stripe_account_id) {
    const bank_result = await Stripe.createBank(stripe_account_id, account_holder_name, account_number, routing_number, country, currency)
    if (typeof bank_result === 'object') {
        const error = bank_result.message
        return error
    }

    const stripe_bank_id = bank_result
    const query = `insert into ${Tables.tbBankAccount} (user_id, bank_name, account_holder_name, account_number, stripe_id, routing_number) 
    values (${user_id}, '${bank_name}', '${account_holder_name}', '${account_number}', '${stripe_bank_id}', '${routing_number}')`
    const result = await Database.execute(query)
    return result.affectedRows === 1
}

async function getAllBanks() {
    const query = `select * from ${Tables.tbBank}`
    const result = await Database.execute(query)
    return result
}

async function getStripeAccountId(user_id) {
    const query = `select stripe_id from ${Tables.tbUser} where user_id = ${user_id}`
    const result = await Database.execute(query)
    if (result.length === 0) { return null }
    return result[0].stripe_id
}

async function searchBank(keyword) {
    const query = `select * from ${Tables.tbBank} where name like '%${keyword}%'`
    const result = await Database.execute(query)
    return result
}

module.exports = {
    addBankAccount,
    getAllBanks,
    getStripeAccountId,
    searchBank,
}
