const BankDb = require('./BankDb')

function sendError(res, message) {
    res.error(400, { error: message })
}

function isValid(value) {
    if (value === null || value === undefined) { return false }
    return !value.nullOrEmpty()
}

async function addAccount(req, res) {
    const user_id = req.user_id
    const {
        bank_name, account_holder_name, account_number, routing_number,
    } = req.body

    let { country, currency } = req.body

    if (isValid(bank_name) === false) { sendError(res, 'Bank name is needed'); return }
    if (isValid(account_holder_name) === false) { sendError(res, 'Account holder name is needed'); return }
    if (isValid(account_number) === false) { sendError(res, 'Account number is needed'); return }

    // set default
    // if (isValid(country) === false) { sendError(res, 'Country is needed'); return }
    // if (isValid(currency) === false) { sendError(res, 'Currency is needed'); return }
    if (isValid(country) === false) { country = 'us' }
    if (isValid(currency) === false) { currency = 'USD' }

    if (isValid(routing_number) === false) { sendError(res, 'Rounting number is needed'); return }

    const stripe_account_id = await BankDb.getStripeAccountId(user_id)
    const result = await BankDb.addBankAccount(user_id, bank_name, account_holder_name, account_number, routing_number, country, currency, stripe_account_id)

    if (typeof result === 'string') {
        res.error(400, { success: false, error: result })
        return
    }

    res.status(201).send({ success: result })
}

async function getAllBanks(req, res) {
    if (Object.keys(req.query).length === 0) {
        const banks = await BankDb.getAllBanks()
        res.send(banks)
        return
    }

    const keyword = req.query.search
    const banks = await BankDb.searchBank(keyword)
    res.send(banks)
}

module.exports = {
    addAccount,
    getAllBanks,
}
