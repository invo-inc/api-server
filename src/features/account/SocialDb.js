const { Database, Tables } = require('../../supports/Importer')
const ServiceProviders = require('../../supports/Enum').ServiceProviders
const AccountDb = require('./AccountDb')

async function addSocialAccount(user_id, service_id, service_token, service_provider) {
    const query = `insert into ${Tables.tbSocialAccount} 
    (user_id, service_id, service_token, service_provider) 
    values (${user_id}, '${service_id}', '${service_token}', '${service_provider}')`
    Database.execute(query)
}

async function checkNeedUpdate(user_id) {
    const query = `select (first_name is null) ||
    (last_name is null) ||
    (email is null) ||
    (address is null) ||
    (state is null) ||
    (dob is null) ||
    (phone is null) ||
    (income is null) ||
    (social_security is null) need_update
    from ${Tables.tbUser} where user_id = ${user_id}`
    const result = await Database.execute(query)
    if (result.length === 0) { return false }
    return result[0].need_update
}

async function socialLogin(raw_user, client_id, lang) {
    const service_id = raw_user.service_id
    let user = await AccountDb.getUserBySocialId(service_id)
    if (user !== null) {
        user = await AccountDb.generateTokenIfNeeded(user, client_id, lang)
        const need_update = await checkNeedUpdate(user.user_id)
        user.need_update = need_update
    } else {
        user = await AccountDb.registerNewSocialUser(raw_user, client_id, lang)
        addSocialAccount(user.user_id, raw_user.service_id, raw_user.service_token, raw_user.service_provider)
        user.need_update = true
    }
    return user
}

function getSocialCheckQuery(user_id, provider) {
    return `select count(user_id) > 0 is_linked from ${Tables.tbSocialAccount} where user_id = ${user_id} and service_provider = '${provider}';`
}
async function checkSocialConnection(user_id) {
    const facebook_check = getSocialCheckQuery(user_id, ServiceProviders.facebook)
    const google_check = getSocialCheckQuery(user_id, ServiceProviders.google)
    const instagram_check = getSocialCheckQuery(user_id, ServiceProviders.instagram)

    const query = `${facebook_check}${google_check}${instagram_check}`
    const result = await Database.execute(query)
    const facebook_linked = result[0][0].is_linked
    const google_linked = result[1][0].is_linked
    const instagram_linked = result[2][0].is_linked
    return { facebook_linked, google_linked, instagram_linked }
}

module.exports = {
    addSocialAccount,
    socialLogin,
    checkSocialConnection,
}
