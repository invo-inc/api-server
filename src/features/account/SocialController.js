const Axios = require('axios')
const Helpers = require('../../supports/Helpers')
const Setting = require('../../supports/Setting')
const Formatter = require('../../supports/Formatter')
const SocialDb = require('./SocialDb')
const ServiceProviders = require('../../supports/Enum').ServiceProviders
const Mongo = require('../../database/MongoConnector')

async function validateFacebook(id, token) {
    let url = Setting.facebook_auth_url
    url = Formatter.replaceText(url, token)
    try {
        const result = await Axios.get(url)
        if (result.status === 400) { return false }
        return result.data.id === id
    } catch (error) {
        return false
    }
}

async function validateGoogle(id, token) {
    let url = Setting.google_auth_url
    url = Formatter.replaceText(url, token)
    try {
        const result = await Axios.get(url)
        if (result.status === 400) { return false }
        return result.data.user_id === id
    } catch (error) {
        return false
    }
}

async function validateService(id, token, provider) {
    switch (provider) {
    case ServiceProviders.facebook: {
        const is_valid = await validateFacebook(id, token)
        return is_valid
    }
    case ServiceProviders.google: {
        const is_valid = await validateGoogle(id, token)
        return is_valid
    }
    default:
        return true
    }
}

async function socialLogin(req, res) {
    const raw = req.body
    const client_id = req.headers.client_id
    const lang = req.lang
    let user = {
        email: raw.email,
        avatar: raw.avatar,
        service_id: raw.service_id,
        service_token: raw.service_token,
        service_provider: raw.service_provider,
    }

    const service_id = (user.service_id || '').toString()
    if (service_id.nullOrEmpty()) {
        res.status(400).send({ error: 'login_service_id is missing' })
        return
    }

    const service_token = (user.service_token || '').toString()
    if (service_token.nullOrEmpty()) {
        res.status(400).send({ error: 'login_service_token is missing' })
        return
    }

    let is_valid_token = false
    const service_provider = user.service_provider
    if (service_provider.nullOrEmpty()) {
        res.status(400).send({ error: 'service_provider is missing' })
        return
    }

    is_valid_token = await validateService(service_id, service_token, service_provider)
    if (is_valid_token === false) {
        res.status(400).send({ error: 'invalid login_service_id and login_service_token' })
        return
    }

    user = await SocialDb.socialLogin(user, client_id, lang)
    delete user.service_token
    res.send(user)
}

async function connectSocial(req, res) {
    const user_id = req.user_id
    const {
        service_provider, service_id, service_token, data,
    } = req.body

    if (Helpers.isValidString(service_id) === false) {
        res.error(400, { error: 'service_id is missing' })
        return
    }

    if (Helpers.isValidString(service_token) === false) {
        res.error(400, { error: 'service_token is missing' })
        return
    }

    if (Helpers.isValidString(service_provider) === false) {
        res.error(400, { error: 'service_provider is missing' })
        return
    }

    const service_validation = validateService(service_id, service_token, service_provider)
    if (service_validation === false) {
        res.error(401, { error: 'Invalid credential' })
        return
    }

    data.service_provider = service_provider
    data.service_token = service_token
    data.user_id = user_id

    Mongo.insert('social', data, (result) => {
        const success = result.insertedCount === 1
        res.send({ success })
        SocialDb.addSocialAccount(user_id, service_id, service_token, service_provider)
    })
}

async function checkSocialConnection(req, res) {
    const user_id = req.user_id
    const result = await SocialDb.checkSocialConnection(user_id)
    res.send(result)
}

module.exports = {
    socialLogin,
    connectSocial,
    checkSocialConnection,
}
