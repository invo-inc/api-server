const Crypto = require('../../supports/Crypto')
const AccountDb = require('./AccountDb')
const L = require('../../lang/LangCenter')
const Helpers = require('../../supports/Helpers')
const Email = require('../email/EmailController')

async function validateEmail(email, res, lang) {
    if (Helpers.isValidString(email) === false) {
        let error = L.keys.missing_warning.i18n(lang)
        const value = L.keys.email.i18n(lang)
        error = `${value} ${error}`
        res.error(400, { error })
        return false
    }

    const email_exist = await Helpers.doesEmailExist(email)
    if (email_exist) {
        const exist = L.keys.exist_email.i18n(lang)
        res.error(400, { exist })
        return false
    }

    const email_validation = Helpers.isValidEmail(email)
    if (email_validation.valid === false) {
        const email_error = email_validation.error.i18n(lang)
        res.error(400, { error: email_error })
        return false
    }

    return true
}

async function validate(user, res, lang) {
    let error = L.keys.missing_warning.i18n(lang)
    function sendMissing(key) {
        const value = key.i18n(lang)
        error = `${value} ${error}`
        res.error(400, { error })
    }

    if (Helpers.isValidString(user.first_name) === false) {
        sendMissing(L.keys.first_name)
        return false
    }

    if (Helpers.isValidString(user.last_name) === false) {
        sendMissing(L.keys.last_name)
        return false
    }

    if (Helpers.isValidString(user.phone) === false) {
        sendMissing(L.keys.phone)
        return false
    }

    if (Helpers.isValidString(user.address) === false) {
        sendMissing(L.keys.address)
        return false
    }

    if (Helpers.isValidString(user.dob) === false) {
        sendMissing(L.keys.dob)
        return false
    }

    if (Helpers.isValidString(user.income) === false) {
        sendMissing(L.keys.income)
        return false
    }

    if (Helpers.isValidString(user.social_security) === false) {
        sendMissing(L.keys.social_security)
        return false
    }

    return true
}

/**
 * @description res.body must contain
 * @description `first_name`, `last_name`, `email`, `password`
 * @description `address`, `state` (optional), `dob` , `income`, `social_security`
 * @description `client_id` (optional)
 */
async function create(req, res) {
    const user = req.body
    user.client_id = req.client_id

    const lang = req.lang
    user.lang = lang

    const password_validation = Helpers.isValidPassword(user.password)
    if (password_validation.valid === false) {
        const error = password_validation.error.i18n(lang)
        res.error(400, { error })
        return
    }

    const is_valid_email = await validateEmail(user.email, res, lang)
    if (is_valid_email === false) { return }

    const is_valid_user = await validate(user, res, lang)
    if (is_valid_user === false) { return }

    let password = user.password
    password = Crypto.encryptPassword(password)
    user.password = password

    const account = await AccountDb.create(user)
    if (account) {
        const message = L.keys.register_success.i18n()
        res.status(201).send({ message })
        // send email
        const user_id = account.user_id
        const verification_url = await AccountDb.createVerifiedUrl(user_id)
        Email.sendConfirmation(`${user.first_name} ${user.last_name}`, user.email, verification_url)
    } else {
        res.send(500)
    }
}

async function checkEmailExist(req, res) {
    const lang = req.lang
    const email = req.body.email || ''
    if (Helpers.isValidString(email) === false) {
        const error = `${'email'.i18n(lang)} ${L.keys.missing_warning.i18n(lang)}`
        res.error(400, { error })
        return
    }

    const email_validation = Helpers.isValidEmail(email)
    if (email_validation.valid === false) {
        const error = email_validation.error.i18n(lang)
        res.error(400, { error })
        return
    }

    const email_exist = await Helpers.doesEmailExist(email)
    if (email_exist) {
        const message = L.keys.exist_email.i18n(lang)
        res.send({ available: false, message })
        return
    }
    res.send({ available: true })
}

async function login(req, res) {
    const lang = req.lang
    const client_id = req.client_id

    const email = req.body.email || ''
    if (Helpers.isValidString(email) === false) {
        const error = `${'email'.i18n(lang)} ${L.keys.missing_warning.i18n(lang)}`
        res.error(400, { error })
        return
    }

    const email_validation = Helpers.isValidEmail(email)
    if (email_validation.valid === false) {
        const error = email_validation.error.i18n(lang)
        res.error(400, { error })
        return
    }

    let password = req.body.password || ''
    if (password === '') {
        const error = L.keys.invalid_email_password.i18n(lang)
        res.error(400, { error })
        return
    }

    password = Crypto.encryptPassword(password)

    const result = await AccountDb.login(email, password, client_id, lang)
    if (result) {
        res.send(result)
        return
    }

    const error = L.keys.invalid_email_password.i18n(lang)
    res.error(400, { error })
}

async function addDetailToSocialAccount(req, res) {
    const data = req.body
    const lang = req.lang
    const user_id = req.user_id
    const old_email = data.old_email || ''

    const email = data.email
    const user = {
        user_id,
        first_name: data.first_name || '',
        last_name: data.last_name || '',
        email,
        address: data.address || '',
        dob: data.dob || '',
        phone: data.phone || '',
        income: data.income || '',
        state: data.state || '',
        social_security: data.social_security || '',
    }

    if (old_email !== email) {
        if (await validateEmail(email, res, lang) === false) { return }
    }

    const is_valid_user = validate(user, res, lang)
    if (is_valid_user === false) { return }

    const success = await AccountDb.addDetailToSocialAccount(user)
    if (success === true) {
        res.send({ message: L.keys.account_updated.i18n(lang) })
        const name = `${data.first_name} ${data.last_name}`
        const confirmation_url = await AccountDb.createUpdateConfirmationUrl(user_id, email)
        Email.sendUpdateConfirmation(name, email, confirmation_url)
    } else {
        res.error(500, { error: 'Can\'t update now' })
    }
}

async function verify(req, res) {
    const code = req.params.code
    const lang = req.lang
    if (Helpers.isValidString(code) === false) {
        res.error(400, { error: 'invalid_verify_code'.i18n(lang) })
        return
    }

    const success = await AccountDb.verify(code)
    if (success === false) {
        res.error(400, { error: 'invalid_verify_code'.i18n(lang) })
        return
    }
    res.send({ message: L.keys.acc_activated.i18n() })
}

async function confirmEmail(req, res) {
    const code = decodeURIComponent(req.query.code)
    const email = decodeURIComponent(req.query.email)
    const lang = req.lang
    if (Helpers.isValidString(code) === false) {
        res.error(400, { error: 'invalid_verify_code'.i18n(lang) })
        return
    }

    const success = await AccountDb.confirmUpdate(code, email)
    if (success === false) {
        res.error(400, { error: 'invalid_verify_code'.i18n(lang) })
        return
    }
    res.send({ message: L.keys.email_confirmed.i18n() })
}

async function resendConfirmation(req, res) {
    const lang = req.lang
    const user_id = req.user_id
    const result = await AccountDb.resendConfirmation(user_id)
    if (result.error) {
        res.error(400, { error: result.error })
        return
    }

    const { name, url, email } = result
    Email.sendConfirmation(`${name}`, email, url)
    res.send({ message: 'send_success'.i18n(lang) })
}

/**
 * Reset password way is different with mobile and web
 * - Send a url to reset and set new pass for web
 * - Send a code to validate reset and set new pass for mobile
 */
async function requestResetPassword(req, res) {
    const lang = req.lang
    if (Helpers.isValidEmail(req.body.email) === false) {
        res.error(400, { error: 'invalid_email'.i18n(lang) })
        return
    }
    const email = req.body.email
    const user = await AccountDb.getUserByEmail(email)
    if (!user) {
        // just ignore request
        res.send({ message: 'send_success'.i18n(lang) })
        return
    }

    const code = await AccountDb.createResetPasswordCode(user.user_id)
    Email.sendResetPassword(`${user.first_name} ${user.last_name}`, user.email, code)
    res.send({ message: 'send_success'.i18n(lang) })
}

/**
 * Reset password different with mobile and web
 */
async function setNewPassword(req, res) {
    const lang = req.lang
    const code = req.body.code || ''
    if (code.nullOrEmpty()) {
        res.error(400, { error: 'Code is missing' })
        return
    }

    let password = req.body.password || ''
    if (password.nullOrEmpty()) {
        res.error(400, { error: 'Password is missing' })
        return
    }

    const validPassword = Helpers.isValidPassword(password)
    if (validPassword === false) {
        const error = L.keys.password_rule
        res.error(400, { error })
        return
    }

    password = Crypto.encryptPassword(password)
    const result = await AccountDb.setNewPassword(code, password)
    if (result === true) {
        res.send(200, { message: L.keys.login_with_new_pass.i18n(lang) })
    } else {
        res.error(400, { error: 'Reset code is invalid' })
    }
}

async function getDetail(req, res) {
    const user_id = req.user_id
    const user = await AccountDb.getFullDetail(user_id)
    res.send(user)
}

module.exports = {
    create,
    login,
    checkEmailExist,
    addDetailToSocialAccount,
    verify,
    resendConfirmation,
    requestResetPassword,
    setNewPassword,
    confirmEmail,
    getDetail,
}
