const { Database, Tables } = require('../../supports/Importer')
const Crypto = require('../../supports/Crypto')
const Formatter = require('../../supports/Formatter')
const UserStatus = require('../../supports/Enum').UserStatus
const MagicPurpose = require('../../supports/Enum').MagicCodePurpose
const Helpers = require('../../supports/Helpers')
const Setting = require('../../supports/Setting')
const ServiceProviders = require('../../supports/Enum').ServiceProviders

/**
 * @description data must contain
 * @description `user_id`, `access_token`, `client_id`, `expired`, `lang`
 */
async function addToken(user_id, client_id, lang) {
    const access_token = Crypto.generateToken()
    const expired = Crypto.getTokenExpiration()
    const new_client_id = client_id || Crypto.generateClientId()

    const now = new Date().unix()
    const insertToken = `insert into ${Tables.tbDevice} 
    (user_id, access_token, client_id, expired_at, lang, created_at)
    values (${user_id}, '${access_token}', '${new_client_id}', '${expired}', '${lang}', ${now})`
    const result = await Database.execute(insertToken)
    result.access_token = access_token
    result.client_id = new_client_id
    return result
}

/**
 * @description account must contain
 * @description `fist_name`, `last_name`, `email`, `password`
 * @description `address`, `state` (optional), `dob` , `income`, `social_security`
 * @description `client_id` (optional)
 * @description `lang` (optional)
 */
async function create(account) {
    const user = account
    const now = new Date().unix()
    const query = `insert into ${Tables.tbUser} 
    (first_name, last_name, email, password, address, state, dob, phone, income, social_security, created_at) 
    values ('${user.first_name}', '${user.last_name}', '${user.email}', '${user.password}', '${user.address}', '${user.state}', '${user.dob}', '${user.phone}', '${user.income}', '${user.social_security}', ${now})`
    const result = await Database.execute(query)
    const user_id = result.insertId
    user.user_id = user_id
    delete user.password
    return user
}

async function getToken(user_id, client_id, lang) {
    const query = `select access_token from ${Tables.tbDevice} where user_id = ${user_id} and client_id = '${client_id}'`
    const token = await Database.execute(query)
    if (token.length === 1) {
        const access_token = token[0].access_token
        return { access_token, client_id }
    }
    return addToken(user_id, client_id, lang)
}

async function generateTokenIfNeeded(data, client_id, lang) {
    const user = data
    const user_id = user.user_id
    if (client_id === null) {
        const token = addToken(user_id, client_id, lang)
        if (token.affectedRows > 0) {
            user.access_token = token.access_token
            user.client_id = token.client_id
        }
    } else {
        const token = await getToken(user_id, client_id, lang)
        user.access_token = token.access_token
        user.client_id = token.client_id
    }
    return user
}

/**
 * @param password must be encrypted
 * @param client_id optional
 */
async function login(email, password, client_id, lang) {
    const query = `select u.user_id, u.email, u.first_name, u.last_name, u.address, u.scopes
    from ${Tables.tbUser} u
    where u.email = '${email}' and u.password = '${password}'`

    const result = await Database.execute(query)
    if (result.length === 1) {
        const user = await generateTokenIfNeeded(result[0], client_id, lang)
        return user
    }
    return null
}

async function getUserBySocialId(service_id) {
    const query = `select user_id, first_name, last_name, email 
    from ${Tables.tbUser} where service_id = '${service_id}'`
    const result = await Database.execute(query)
    if (result.length !== 1) { return null }
    return result[0]
}

async function registerNewSocialUser(account, client_id, lang) {
    const now = new Date().unix()
    const user = account
    const query = `insert into ${Tables.tbUser} 
    (service_id, service_provider, service_token, avatar, status, created_at) 
    values ('${account.service_id}', '${account.service_provider}', '${account.service_token}', '${account.avatar}', 'active', ${now})`
    const result = await Database.execute(query)
    const user_id = result.insertId
    user.user_id = user_id

    const token = await addToken(user_id, client_id, lang)
    user.access_token = token.access_token
    user.client_id = token.client_id
    return user
}


async function addDetailToSocialAccount(data) {
    const user = data
    const user_id = user.user_id
    delete user.user_id
    delete user.email
    const update_query = Formatter.generateUpdateQuery(user)
    const query = `update ${Tables.tbUser} set ${update_query} where user_id = ${user_id}`
    const result = await Database.execute(query)
    return result.affectedRows === 1
}

async function verify(code) {
    const query = `select * from ${Tables.tbMagicCode} where code = '${code}'`
    const result = await Database.execute(query)
    if (result.length !== 1) { return false }
    const item = result[0]
    const now = new Date().unix()
    if (now > item.expired_at || item.code === null) { return false }

    const user_id = item.user_id
    const update_user = `update ${Tables.tbUser} set status = '${UserStatus.active}' where user_id = ${user_id}`
    Database.execute(update_user)

    const delete_code = `delete from ${Tables.tbMagicCode} where code = '${code}'`
    Database.execute(delete_code)

    return true
}

async function confirmUpdate(code, email) {
    const query = `select * from ${Tables.tbMagicCode} where code = '${code}'`
    const result = await Database.execute(query)
    if (result.length !== 1) { return false }
    const item = result[0]
    const now = new Date().unix()
    if (now > item.expired_at || item.code === null) { return false }

    const user_id = item.user_id
    const update_user = `update ${Tables.tbUser} set email = '${email}' where user_id = ${user_id}`
    Database.execute(update_user)

    const delete_code = `delete from ${Tables.tbMagicCode} where code = '${code}'`
    Database.execute(delete_code)

    return true
}

async function createVerifiedUrl(user_id) {
    const code = Helpers.randomizeString(100)
    const { base_url, root_url } = Setting
    const url = `${root_url}${base_url}/account/verify/${code}`
    Helpers.addMagicCode(user_id, code, MagicPurpose.verify_account)
    return url
}

async function createUpdateConfirmationUrl(user_id, email) {
    let code = Helpers.randomizeString(100)
    Helpers.addMagicCode(user_id, code, MagicPurpose.update_email)
    code = encodeURIComponent(`${code}`)
    const encoded_email = encodeURI(`${email}`)
    const { base_url, root_url } = Setting
    const url = `${root_url}${base_url}/account/confirm_email?code=${code}&email=${encoded_email}`
    return url
}

async function resendConfirmation(user_id, lang) {
    let query = `select status from ${Tables.tbUser} where user_id = ${user_id}`
    let result = await Database.execute(query)
    if (result.length !== 1) { return { error: 'invalid user' } }
    result = result[0]
    if (result.status !== UserStatus.pending) {
        return { error: 'acc_alr_activated'.i18n(lang) }
    }

    query = `delete from ${Tables.tbMagicCode} where user_id = ${user_id} and purpose = '${MagicPurpose.verify_account}'`
    await Database.execute(query)
    const url = await createVerifiedUrl(user_id)
    const name_query = `select first_name, last_name, email from ${Tables.tbUser} where user_id = ${user_id}`
    result = await Database.execute(name_query)
    let name = 'Valued User'
    let email = ''
    if (result.length === 1) {
        const user = result[0]
        name = `${user.first_name} ${user.last_name}`
        email = user.email
    }

    return { url, name, email }
}

async function getUserByEmail(email) {
    const query = `select user_id, first_name, last_name, email 
    from ${Tables.tbUser} where email = '${email}' and service_provider = '${ServiceProviders.email}'`
    const result = await Database.execute(query)
    if (result.length !== 1) { return null }
    return result[0]
}

async function createResetPasswordCode(user_id) {
    const query = `delete from ${Tables.tbMagicCode} where user_id = ${user_id} and purpose = '${MagicPurpose.reset_password}'`
    await Database.execute(query)

    const code = Helpers.randomizeString(6)
    Helpers.addMagicCode(user_id, code, MagicPurpose.reset_password)
    return code
}

async function setNewPassword(code, password) {
    let query = `select user_id from ${Tables.tbMagicCode} where code = '${code}' and purpose = '${MagicPurpose.reset_password}'`
    let result = await Database.execute(query)
    if (result.length !== 1) { return false }

    const user_id = result[0].user_id
    query = `update ${Tables.tbUser} set password = '${password}' where user_id = ${user_id}`
    result = await Database.execute(query)
    return result.affectedRows === 1
}

async function getFullDetail(user_id) {
    const query = `select user_id, first_name, last_name, email, address, state, dob, phone, income, social_security from ${Tables.tbUser} where user_id = ${user_id}`
    const result = await Database.execute(query)
    if (result.length === 0) { return null }
    return result[0]
}

module.exports = {
    create,
    addDetailToSocialAccount,
    login,
    generateTokenIfNeeded,
    verify,
    createVerifiedUrl,
    resendConfirmation,
    getUserByEmail,
    createResetPasswordCode,
    setNewPassword,
    getUserBySocialId,
    createUpdateConfirmationUrl,
    confirmUpdate,
    registerNewSocialUser,
    getFullDetail,
}
