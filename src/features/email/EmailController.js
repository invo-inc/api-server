const Mailer = require('nodemailer')
const Mailgen = require('mailgen')
const { mail_server, mail_password } = require('../../supports/Setting')
const Setting = require('../../supports/Setting')

async function sendEmail(mail) {
    const transporter = Mailer.createTransport({
        service: 'gmail',
        auth: {
            user: mail_server,
            pass: mail_password,
        },
    })

    transporter.sendMail(mail, (error, response) => {
        if (error) {
            console.log(error)
        } else {
            console.log(`Message sent: ${response.message}`)
        }
        transporter.close()
    })
}

async function sendConfirmation(user_name, user_email, confirm_url) {
    const mailGenerator = new Mailgen({
        theme: 'default',
        product: {
            name: Setting.product_name,
            link: Setting.product_link,
            logo: Setting.product_logo,
        },
    })

    const product_name = Setting.product_name
    const email = {
        body: {
            name: user_name,
            intro: `Welcome to ${product_name}! We're very excited to have you on board.`,
            action: {
                instructions: `To get started with ${product_name}, please click here:`,
                button: {
                    color: '#22BC66',
                    text: 'Confirm your account',
                    link: confirm_url,
                },
            },
            outro: 'Need help, or have questions? Just reply to this email, we\'d love to help.',
        },
    }

    const emailBody = mailGenerator.generate(email)
    const email_detail = {
        from: `${product_name} ${mail_server}`,
        to: user_email,
        subject: 'Activate your INVO account',
        html: emailBody,
    }

    sendEmail(email_detail)
}

async function sendUpdateConfirmation(user_name, user_email, confirm_url) {
    const mailGenerator = new Mailgen({
        theme: 'default',
        product: {
            name: Setting.product_name,
            link: Setting.product_link,
            logo: Setting.product_logo,
        },
    })

    const product_name = Setting.product_name
    const email = {
        body: {
            name: user_name,
            intro: 'You have just requested to update your email.',
            action: {
                instructions: 'Please click button below to confirm your request',
                button: {
                    color: '#22BC66',
                    text: 'Confirm your email',
                    link: confirm_url,
                },
            },
            outro: 'Need help, or have questions? Just reply to this email, we\'d love to help.',
        },
    }

    const emailBody = mailGenerator.generate(email)
    const email_detail = {
        from: `${product_name} ${mail_server}`,
        to: user_email,
        subject: 'Confirm new email',
        html: emailBody,
    }

    sendEmail(email_detail)
}


async function sendResetPassword(user_name, user_email, code) {
    const mailGenerator = new Mailgen({
        theme: 'default',
        product: {
            name: Setting.product_name,
            link: Setting.product_link,
            logo: Setting.product_logo,
        },
    })

    const product_name = Setting.product_name
    const email = {
        body: {
            name: user_name,
            intro: `We received an account recovery request from you for ${user_name}. 
            Please enter ${code} to your confirmation code on mobile`,
            outro: 'Need help, or have questions? Just reply to this email, we\'d love to help.',
        },
    }

    const emailBody = mailGenerator.generate(email)
    const email_detail = {
        from: `${product_name} ${mail_server}`,
        to: user_email,
        subject: 'Reset password',
        html: emailBody,
    }

    sendEmail(email_detail)
}

async function sendSponsorship(email_address, to_name, content) {
    const mailGenerator = new Mailgen({
        theme: 'default',
        product: {
            name: Setting.product_name,
            link: Setting.product_link,
            logo: Setting.product_logo,
        },
    })

    const product_name = Setting.product_name
    const email = {
        body: {
            name: to_name,
            intro: content,
            outro: 'Need help, or have questions? Just reply to this email, we\'d love to help.',
        },
    }

    const emailBody = mailGenerator.generate(email)
    const email_detail = {
        from: `${product_name} ${mail_server}`,
        to: email_address,
        subject: 'Sponsorship',
        html: emailBody,
    }

    sendEmail(email_detail)
}

module.exports = {
    sendConfirmation,
    sendResetPassword,
    sendUpdateConfirmation,
    sendSponsorship,
}
