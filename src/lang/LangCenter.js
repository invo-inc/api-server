const En = require('./En')
const Tables = require('../database/Tables')
const Database = require('../database/Connector')

const supported_langs = {
    en: En,
}

module.exports.keys = {
    exist_email: 'exist_email',
    password_length: 'password_length',
    missing_warning: 'missing_warning',
    email: 'email',
    phone: 'phone',
    first_name: 'first_name',
    last_name: 'last_name',
    password: 'password',
    address: 'address',
    dob: 'dob',
    income: 'income',
    social_security: 'social_security',
    invalid_email_password: 'invalid_email_password',
    invalid_email: 'invalid_email',
    register_success: 'register_success',
    account_updated: 'account_updated',
    invalid_verify_code: 'invalid_verify_code',
    acc_activated: 'acc_activated',
    send_success: 'send_success',
    acc_alr_activated: 'acc_alr_activated',
    confirm_intro: 'confirm_intro',
    confirm_instruction: 'confirm_instruction',
    confirm_button: 'confirm_button',
    confirm_outro: 'confirm_outro',
    regards: 'regards',
    support_team: 'support_team',
    activate_account: 'activate_account',
    password_rule: 'password_rule',
    login_with_new_pass: 'login_with_new_pass',
    email_confirmed: 'email_confirmed',
}

/**
 * @desc Get string by language
 * @param key of the text
 * @param lang
 * @return String in selected language
 */
function getText(key, lang) {
    const dict = supported_langs[lang].texts
    return dict[key]
}
module.exports.getText = getText

/**
 * @desc Get Genius language from db
 * @param user_id
 * @return Language sign: en, vi
 */
async function getGeniusLang(user_id) {
    const query = `select lang from ${Tables.tbDevice} where user_id = ${user_id}`
    const result = await Database.execute(query)
    if (result == null) { return 'en' }
    if (result.length === 0) { return 'en' }
    return result[0].lang
}
module.exports.getGeniusLang = getGeniusLang
