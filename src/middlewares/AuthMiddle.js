const Crypto = require('../supports/Crypto')
const AuthDb = require('./AuthDb')

async function auth(req) {
    const token_invalid = 'Authorization token is not accepted'
    const token = req.headers.authorization
    const client_id = req.headers.client_id
    if (token == null) {
        const error = new Error('Authorization header is required')
        error.status = 401
        return error
    }

    if (client_id == null) {
        const error = new Error('Client_Id header is required')
        error.status = 401
        return error
    }

    // CPU validation
    if (!Crypto.isTokenValid(token)) {
        const error = new Error(token_invalid)
        error.status = 401
        return error
    }

    // Database validation
    const token_detail = await AuthDb.getTokenDetails(client_id, token)
    if (token_detail == null) {
        const error = new Error('client_id and token are not matched')
        error.status = 401
        return error
    }

    const current_time = new Date().unix()
    const is_expired = parseFloat(current_time) > parseFloat(token_detail.expire_at)
    if (is_expired) {
        const error = new Error('The token has expired')
        error.status = 401
        return error
    }

    const user_id = parseInt(token_detail.user_id)
    let scopes = (token_detail.scopes || '').split(',')

    if (isNaN(user_id)) {
        const error = new Error('No user associate with this token')
        error.status = 401
        return error
    }

    if (scopes.length === 0 || scopes[0] === 'NULL') { scopes = ['user'] }

    req.user_id = user_id
    req.scopes = scopes
    req.language = req.headers['accept-language'] || 'en'
    return null
}

function checkUserScope(scopes, scope) {
    if (scopes.indexOf(scope) === -1) {
        const forbidden_error = new Error()
        forbidden_error.status = 403
        forbidden_error.message = 'You don\'t have permission to perform this action'
        return forbidden_error
    }
    return null
}

async function authUser(req) {
    const error = await auth(req)
    if (error) { return error }

    const scopes = req.scopes
    const havePermission = checkUserScope(scopes, 'user')
    return havePermission
}

module.exports = {
    authUser,
}
