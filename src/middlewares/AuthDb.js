const Tables = require('../database/Tables')
const Database = require('../database/Connector')

async function getTokenDetails(client_id, token) {
    const query = `select u.user_id, expired_at, scopes from ${Tables.tbUser} u, ${Tables.tbDevice} d where access_token = '${token}' and client_id = '${client_id}' and u.user_id = d.user_id`
    const token_details = await Database.execute(query)
    if (token_details.length === 0) {
        return null
    }
    return token_details[0]
}

module.exports = {
    getTokenDetails,
}
