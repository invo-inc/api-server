function extendResponse(res) {
    res.error = function sendError(code, error) {
        if (error === null) {
            res.sendStatus(code); return
        }
        res.status(code).send(error)
    }
}

module.exports = {
    extendResponse,
}
