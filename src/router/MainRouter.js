const express = require('express')
const BaseUrl = require('../supports/Setting').base_url
const AuthRouter = require('./AuthRouter')
const extendResponse = require('./Responder').extendResponse
const Helpers = require('../supports/Helpers')

const router = express.Router()

function api(url) { return `${BaseUrl}${url}` }

router.use((req, res, next) => {
    extendResponse(res)
    next()
})

router.use((err, req, res, next) => {
    Helpers.sendError(err, res)
})

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
const account = require('../features/account/AccountController')
const social = require('../features/account/SocialController')

router.post(api('/register'), AuthRouter.authPublic(account.create))
router.post(api('/login'), AuthRouter.authPublic(account.login))
router.post(api('/social_login'), AuthRouter.authPublic(social.socialLogin))
router.post(api('/check_email_exist'), AuthRouter.authPublic(account.checkEmailExist))

router.get(api('/account/verify/:code'), AuthRouter.authPublic(account.verify))
router.get(api('/account/resend_confirmation'), AuthRouter.authUser(account.resendConfirmation))
router.get(api('/account'), AuthRouter.authUser(account.getDetail))
router.put(api('/account/update'), AuthRouter.authUser(account.addDetailToSocialAccount))
router.get(api('/account/confirm_email'), AuthRouter.authPublic(account.confirmEmail))

router.post(api('/account/reset_password'), AuthRouter.authPublic(account.requestResetPassword))
router.post(api('/account/set_new_password'), AuthRouter.authPublic(account.setNewPassword))

router.post(api('/user/social'), AuthRouter.authUser(social.connectSocial))
router.get(api('/user/social/check'), AuthRouter.authUser(social.checkSocialConnection))

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
const sponsor = require('../features/sponsor/SponsorController')

router.post(api('/user/sponsors'), AuthRouter.authUser(sponsor.addSponsor))
router.get(api('/user/sponsors'), AuthRouter.authUser(sponsor.getSponsors))
router.get(api('/user'), AuthRouter.authUser(sponsor.findUserByEmail))
router.delete(api('/user/sponsors/:id'), AuthRouter.authUser(sponsor.remove))
router.get(api('/user/sponsors/:id/resend'), AuthRouter.authUser(sponsor.resendRequestSponsor))
router.put(api('/user/sponsors/:id/accept'), AuthRouter.authUser(sponsor.acceptRequest))
router.put(api('/user/sponsors/:id/reject'), AuthRouter.authUser(sponsor.rejectRequest))
router.put(api('/user/sponsors/:id/cancel'), AuthRouter.authUser(sponsor.cancelRequest))

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
const bank = require('../features/banks/BankController')

router.get(api('/banks'), AuthRouter.authUser(bank.getAllBanks))
router.post(api('/user/banks'), AuthRouter.authUser(bank.addAccount))

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
const contact = require('../features/contact/ContactController')

router.post(api('/user/contacts'), AuthRouter.authUser(contact.addContacts))

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

router.get('/test', (req, res) => {
    const Social = require('../features/account/SocialController')

    const result = Social.connectSocial(req, res)
})

module.exports = router
