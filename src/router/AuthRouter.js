const AuthMiddle = require('../middlewares/AuthMiddle')
const Helpers = require('../supports/Helpers')

function getHeader(req) {
    const client_id = req.headers.client_id
    req.client_id = client_id
    req.lang = req.headers['accept-language'] || 'en'
    req.lang = req.lang.length > 2 ? req.lang.substring(0, 2) : req.lang
}

const authPublic = fn => (req, res, next) => {
    getHeader(req)
    Promise.resolve(fn(req, res, next))
        .catch(next)
}

const authUser = fn => async (req, res, next) => {
    getHeader(req)
    const error = await AuthMiddle.authUser(req)
    if (error) {
        Helpers.sendError(error, res)
        return
    }
    Promise.resolve(fn(req, res, next)).catch(next)
}

module.exports = {
    authPublic,
    authUser,
}
