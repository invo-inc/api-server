const Database = require('../database/Connector')
const Tables = require('../database/Tables')
const Mongo = require('../database/MongoConnector')

module.exports = {
    Database, Tables, Mongo,
}
