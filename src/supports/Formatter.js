function clearNulls(data) {
    Object.keys(data).every((key) => {
        const obj = data
        const value = data[key]
        if (value === null || value === undefined) {
            delete obj[key]
        }
        if (typeof value === 'string' && (value.toUpperCase() === 'NULL'
            || value.toLowerCase() === 'undefined')) {
            delete obj[key]
        }

        return obj
    })
    return data
}

function generateUpdateQuery(data) {
    let query = ''

    Object.keys(data).every((key) => {
        const value = data[key]
        if (value === null || value === undefined) { return '' }
        if (typeof value === 'number') {
            query += `${key} = ${value}`
        } else {
            query += `${key} = '${value}'`
        }
        query += ','
        return query
    })

    query = query.substring(0, query.length - 1)
    return query
}

function replaceText(inText, ...withTexts) {
    let mainText = inText
    const texts = withTexts
    for (let i = 0; i <= texts.length; i += 1) {
        const text = texts[i]
        const template = `%@${i + 1}`
        mainText = mainText.replace(template, text)
    }
    return mainText
}

module.exports = {
    clearNulls,
    generateUpdateQuery,
    replaceText,
}
