module.exports = {
    MagicCodePurpose: {
        verify_account: 'verify_account',
        reset_password: 'reset_password',
        update_email: 'update_email',
    },
    UserStatus: {
        pending: 'pending',
        active: 'active',
        inactive: 'inactive',
    },
    ServiceProviders: {
        email: 'email',
        facebook: 'facebook',
        google: 'google',
        instagram: 'instagram',
    },
    SponsorStatus: {
        pending: 'pending',
        accepted: 'accepted',
        rejected: 'rejected',
        cancelled: 'cancelled',
        removed: 'removed',
    },
    SponsorType: {
        sponsor: 'sponsor',
        sponsorship: 'sponsorship',
    },
    BankAccountStatus: {
        active: 'active',
        inactive: 'inactive',
        removed: 'removed',
    },
}
