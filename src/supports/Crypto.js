const HmacSHA1 = require('crypto-js').HmacSHA1
const PBKDF2 = require('crypto-js').PBKDF2
const Helpers = require('./Helpers')

const key = 'g1UuVb3cLxJugSeiWvnHEsHnKiILS8jfWx8cupDw0y5TdxjdFGLA7akibuP4UWUaHl2Sxm0TnVLmQlxPvs7dCGllxm/K72Z5'
const key_2 = '718ef1beeca849fd8edbcfc36104de2557e8d6e757f63668357000b643325768a657920916e6baa31bdd2dd04b45c85f3c1abb43e41be339a79988e09cd7008044e2bb3fff69e'
const hard_string = '+v2we+T3ydFsTh5OBK6u+MbEX7jV/6JqxnUC2ceZGTBvN8a3XV1tpjWVjtbU0j2zoMyUn+uQjdsq0UGk4O47RSILBuzEMKD0pvahuw+Ipy23snwFB/DKoUHo5yxs2Hsx8TZnghMylBxq9olI+EXbCuhKfqNsvpjR7TLNU4XD7tKw1PEq6alrqilzfYsELf7kqO8wpuKLf1IpsVptICpUy8GnraPKkeCJjnGGAPpZnb5Li2/32SWYEOZSdnjSifUuHWarsaW1orN2eH+FAivUBKGYNWbtLkTkt66JC6GRKSEu3LbkVA52GAT/YZ/3DkT4ej2OYN87e0Rw59elzJ/euFrJu74YNMiiykb4GEiKanqFhGm3Tz4KKnReA7vzepTvdgHYvPhwaMXkF+LAIyS12hAIzks0qp0lR4TeTtm1/OzDMQVVWpabI7asoFC9L82CvxQpUG54WDAaggZUESQ8AHiUe3wkVmsa3UsJdHd5UoAIv6ghqoxgNa0ojUz7M6ZKgNsgozdkYGvaD1u5EiZItI2+vqvvS1ciJ/LjMeFgayB0Pxo9i+aMciN+cSurfkVbX/esxEf7wNbGg8Z3gJK3fd7NuDKGhLn/AXJdOhWG2RxAiI7ruRxvwyWgxA+wEo7MLiYVyKhIfsihC6GRofDafP0JGVZ3OEz76AihT8FVW7kwiSRlW7tOHRlZ/1Hp2IpkOaARM22FjYRiAqyQXdi2ZZugUg3vC3oUwTK3NVrmbi/IGNHe9Sh3z8eajS6KFmg3iRoXLyP+wveOKulS1UpQ389KffsSjfGRIpJx+WpEMBU='

function randomHexLikeString(length) {
    let text = ''
    const possible = 'abcdef0123456789'
    for (let i = 0; i < length; i += 1) {
        text += possible.charAt(Math.floor(Math.random() * possible.length))
    }
    return text
}

function generateToken() {
    const start_index = Helpers.randomNumber(100, hard_string.length - 1)
    const end_index = Helpers.randomNumber(start_index, hard_string.length)
    const password = hard_string.substring(start_index, end_index)
    const hash = HmacSHA1(password, key).toString()
    const random_string_length = Helpers.randomNumber(1, 5)
    const random_string = randomHexLikeString(random_string_length)
    let token = hash.slice(0, random_string_length)
    token += random_string + hash.slice(random_string_length)
    token = start_index.toString() + random_string_length.toString() + token + end_index.toString()
    return token
}

function generateClientId() {
    return randomHexLikeString(32)
}

function isTokenValid(token) {
    const start_index = parseInt(token.slice(0, 3))
    const end_index = parseInt(token.slice(token.length - 3, token.length))
    if (isNaN(start_index) || isNaN(end_index)) {
        return false
    }

    const random_number = parseInt(token.slice(3, 4))
    const token_with_string = token.slice(4, token.length - 3)
    let password = token_with_string.slice(0, random_number)
    password += token_with_string.slice(random_number + random_number, token_with_string.length)
    if (password === '') {
        return false
    }

    const string_to_encrypt = hard_string.substring(start_index, end_index)
    const actual_hash = HmacSHA1(string_to_encrypt, key).toString()
    return password === actual_hash
}

function encryptPassword(password) {
    let new_pass = PBKDF2(password, key)
    new_pass = PBKDF2(password, key_2)
    return new_pass
}

function getTokenExpiration() {
    let expired_time = new Date()
    expired_time.setDate(expired_time.getDate() + 30)
    expired_time = expired_time.unix()
    return expired_time
}

module.exports = {
    generateToken,
    isTokenValid,
    generateClientId,
    encryptPassword,
    getTokenExpiration,
}
