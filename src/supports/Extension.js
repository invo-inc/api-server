// String.prototype.nullOrEmpty = function nullOrEmpty() {
//     return this.length === 0
// }

Object.prototype.nullOrEmpty = function nullOrEmpty() {
    if (this === null || this === undefined) { return true }
    if (this.length === 0) { return true }
    if (typeof this === 'object') { return Object.keys(this).length === 0 }
    if (typeof this === 'string') { return this.length === 0 }
    return true
}

Date.prototype.addDays = function addDays(days) {
    const date = new Date(this.valueOf())
    date.setDate(date.getDate() + days)
    return date
}

Date.prototype.unix = function unix() {
    return Math.floor(this / 1000)
}

String.prototype.isValidEmail = function isValidEmail() {
    const re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
    return re.test(String(this).toLowerCase())
}

const Lang = require('../lang/LangCenter')

String.prototype.i18n = function i18n(lang) {
    return Lang.getText(this, lang || 'en')
}

String.prototype.formatString = function replaceText(...withTexts) {
    let mainText = this
    const texts = withTexts
    for (let i = 0; i <= texts.length; i += 1) {
        const text = texts[i]
        const template = `%@${i + 1}`
        mainText = mainText.replace(template, text)
    }
    return mainText
}


Object.prototype.clearNulls = function clearNulls() {
    Object.keys(this).every((key) => {
        const obj = this
        const value = this[key]
        if (value === null || value === undefined) {
            delete obj[key]
        }
        if (typeof value === 'string' && (value.toUpperCase() === 'NULL'
            || value.toLowerCase() === 'undefined')) {
            delete obj[key]
        }

        return obj
    })
    return this
}
