const L = require('../lang/LangCenter')
const Tables = require('../database/Tables')
const Database = require('../database/Connector')
const Setting = require('./Setting')

function isValidString(string) {
    if (string === null || string === undefined) { return false }
    const is_valid = !(string || '').nullOrEmpty()
    return is_valid
}

function isValidPassword(string) {
    const value = string || ''
    if (value.length < 6) {
        return { valid: false, error: L.keys.password_length }
    }
    // other rules
    return { valid: true }
}

function isValidEmail(string) {
    const value = string || ''
    if (value.isValidEmail()) { return { valid: true } }
    return { valid: false, error: L.keys.invalid_email }
}

async function doesEmailExist(email) {
    const query = `select count(email) > 0 does_exist from ${Tables.tbUser} where email = '${email}'`
    const result = await Database.execute(query)
    return result[0].does_exist
}

function randomNumber(min, max) {
    return Math.floor(Math.random() * (max - min + 1) + min)
}

function randomizeString(count = 0) {
    const hard_string = 'zHbqjLxPQGU9Q+clUkRg6eAPjkohfL2pnva52mWsnIBjyTmOzSA0j20ljxmuq4eMA0ITrpmbWpo0e9Cjt3dwDYzMdWrZNRogRhYE0LuPhOCPpKojWOZs7xBQKwvK0TJ2QGsTFa1gQLRBjoIZn8pNKVWy3yUZRJ+VVr6ufCMK1c3bL2zrVxqTSOSowi05CKBGKUX+lC9jAAz0EfpafQtjyat9zz8g5EhP4Tyx6FQfeo+Z9WeCo7h1GzJQiWFQArtfx1Xmg9BHBNFo+mm1Nuhgn9SWWFuZRcjalkl0fBH3OhvV4OiHqYxBjrU1skGXrvWImFdY1Q8pOn6URWwPT8g4DGBrnLCCZsEsXhsNIAmajtNhqTprWNHHRtYd7XfY8lZ8xv1FhuNT96J3r6j4VLfKPiI35Po+WHadO1iE55maBnTs0YWovNdeMDGeqeh1NjQIeTkQ09AhyzY579LsHamxCLYUltqbWJ4BWudvNDiVrM7kFbBHtS+VEAWEB+WBGRtp+ycgQ6zGRxHWNEyBiHxN2HOYTFgBCH5iyfhV+qjK1HwQAZkwlNJf2bNgnP5W9s4SHEPcVBszmRm5g=='

    const start = randomNumber(1, hard_string.length - count)
    let end = 0
    if (count === 0) {
        end = randomNumber(start, hard_string.length - 1)
    } else {
        end = start + count
    }

    const string = hard_string.substring(start, end)
    return string
}

function getExpiredTime(days) {
    const expired_time = new Date()
    expired_time.setDate(expired_time.getDate() + days)
    return expired_time.unix()
}

async function addMagicCode(user_id, code, purpose) {
    const expired_time = getExpiredTime(1)
    const query = `insert into ${Tables.tbMagicCode} 
    (user_id, code, purpose, expired_at) 
    values (${user_id}, '${code}', '${purpose}', '${expired_time}')
    ON DUPLICATE KEY UPDATE code = '${code}', expired_at = '${expired_time}'`
    await Database.execute(query)
}

function sendError(err, res) {
    if (Setting.isPro() === false) {
        const new_error = err
        const string = JSON.stringify(err, Object.getOwnPropertyNames(new_error))
        const object = JSON.parse(string)
        res.status(err.status || err.statusCode || 500).send(object)
        return
    }
    res.sendStatus(err.status || err.statusCode || 500)
}

module.exports = {
    isValidString,
    isValidPassword,
    isValidEmail,
    randomNumber,
    randomizeString,
    addMagicCode,
    sendError,
    doesEmailExist,
}
